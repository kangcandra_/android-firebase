package com.example.androidfirebase.mahasiswa;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.example.androidfirebase.DashboardActivity;
import com.example.androidfirebase.R;
import com.example.androidfirebase.model.Mahasiswa;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

public class CreateActivity extends AppCompatActivity {

    Button back, simpan;
    EditText nama, nis, jurusan, alamat;
    DatabaseReference databaseReference;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_create);

        nama = findViewById(R.id.et_nama);
        nis = findViewById(R.id.et_nis);
        jurusan = findViewById(R.id.et_jurusan);
        alamat = findViewById(R.id.et_alamat);
        simpan = findViewById(R.id.btn_submit);

        databaseReference = FirebaseDatabase.getInstance().getReference();

        // store logic
        simpan.setOnClickListener(v -> {
            String getNama = nama.getText().toString();
            String getNis = nis.getText().toString();
            String getJurusan = jurusan.getText().toString();
            String getAlamat = alamat.getText().toString();

            if(getNama.isEmpty()){
                nama.setError("Masukan Nama!");
            } else if(getNis.isEmpty())
            {
                nis.setError("Masukan NIS!");
            } else if(getJurusan. isEmpty()){
                jurusan.setError("Masukan Jurusan!");
            } else if(getAlamat.isEmpty())
            {
                alamat.setError("Masukan Alamat!");
            } else{
                Mahasiswa mhs = new Mahasiswa(getNama,getNis, getJurusan,getAlamat);
                databaseReference.child("Mahasiswa").push().setValue(mhs).addOnSuccessListener(suc->{
                    Toast.makeText(this, "Data berhasil disimpan", Toast.LENGTH_SHORT).show();
                    startActivity(new Intent(CreateActivity.this, DashboardActivity.class));
                    finish();
                }).addOnFailureListener(er ->{
                    Toast.makeText(this, "Data Gagal di input!", Toast.LENGTH_SHORT).show();
                });
            }
        });

        back = findViewById(R.id.btn_back);
        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(CreateActivity.this, DashboardActivity.class));
            }
        });
    }
}