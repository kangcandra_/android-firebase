package com.example.androidfirebase.mahasiswa;

import android.app.Dialog;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.DialogFragment;

import com.example.androidfirebase.R;
import com.example.androidfirebase.model.Mahasiswa;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

public class DialogForm extends DialogFragment {
    String nis, nama, jurusan, alamat, key, pilih;
    DatabaseReference databaseReference = FirebaseDatabase.getInstance().getReference();

    public DialogForm(String nis, String nama, String jurusan, String alamat, String key, String pilih) {
        this.nis = nis;
        this.nama = nama;
        this.jurusan = jurusan;
        this.alamat = alamat;
        this.key = key;
        this.pilih = pilih;
    }

    TextView tnama, tnis, tjurusan, talamat;
    Button btn_simpan;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        final View view = inflater.inflate(R.layout.activity_create, container, false);
        tnama = view.findViewById(R.id.et_nama);
        tnis = view.findViewById(R.id.et_nis);
        tjurusan = view.findViewById(R.id.et_jurusan);
        talamat = view.findViewById(R.id.et_alamat);
        btn_simpan = view.findViewById(R.id.btn_submit);

        tnama.setText(nama);
        tnis.setText(nis);
        tjurusan.setText(jurusan);
        talamat.setText(alamat);
        btn_simpan.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String nama = tnama.getText().toString();
                String nis = tnis.getText().toString();
                String jurusan = tjurusan.getText().toString();
                String alamat = talamat.getText().toString();
                if(pilih.equals("Ubah")){
                    databaseReference.child("Mahasiswa").child(key).setValue(new Mahasiswa(nama,nis,jurusan,alamat)).addOnSuccessListener(new OnSuccessListener<Void>() {
                        @Override
                        public void onSuccess(Void unused) {
                            Toast.makeText(view.getContext(),"Berhasil di update", Toast.LENGTH_SHORT).show();
                        }
                    }).addOnFailureListener(new OnFailureListener() {
                        @Override
                        public void onFailure(@NonNull Exception e) {
                            Toast.makeText(view.getContext(),"Data gagal di update", Toast.LENGTH_SHORT).show();
                        }
                    });
                }
            }
        });
        return view;
    }

    @Override
    public void onStart() {
        super.onStart();
        Dialog dialog = getDialog();
        if (dialog != null)
        {
            dialog.getWindow().setLayout(ViewGroup.LayoutParams.MATCH_PARENT,ViewGroup.LayoutParams.WRAP_CONTENT);
        }
    }
}
