package com.example.androidfirebase.model;

public class Dosen {
    private String key;
    private String nama;
    private String nipd;
    private String matkul;
    private String alamat;

    public Dosen(){

    }

    public Dosen(String nama, String nipd, String matkul, String alamat) {
        this.nama = nama;
        this.nipd = nipd;
        this.matkul = matkul;
        this.alamat = alamat;
    }

    public String getKey() {
        return key;
    }

    public void setKey(String key) {
        this.key = key;
    }

    public String getNama() {
        return nama;
    }

    public void setNama(String nama) {
        this.nama = nama;
    }

    public String getNipd() {
        return nipd;
    }

    public void setNipd(String nipd) {
        this.nipd = nipd;
    }

    public String getMatkul() {
        return matkul;
    }

    public void setMatkul(String matkul) {
        this.matkul = matkul;
    }

    public String getAlamat() {
        return alamat;
    }

    public void setAlamat(String alamat) {
        this.alamat = alamat;
    }
}
