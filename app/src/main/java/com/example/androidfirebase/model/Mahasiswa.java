package com.example.androidfirebase.model;

public class Mahasiswa {
    private String key;
    private String nama;
    private String nis;
    private String jurusan;
    private String alamat;

    public Mahasiswa(){

    }

    public Mahasiswa(String nama, String nis, String jurusan, String alamat) {
        this.nama = nama;
        this.nis = nis;
        this.jurusan = jurusan;
        this.alamat = alamat;
    }

    public String getKey() {
        return key;
    }

    public void setKey(String key) {
        this.key = key;
    }

    public String getNama() {
        return nama;
    }

    public void setNama(String nama) {
        this.nama = nama;
    }

    public String getNis() {
        return nis;
    }

    public void setNis(String nis) {
        this.nis = nis;
    }

    public String getJurusan() {
        return jurusan;
    }

    public void setJurusan(String jurusan) {
        this.jurusan = jurusan;
    }

    public String getAlamat() {
        return alamat;
    }

    public void setAlamat(String alamat) {
        this.alamat = alamat;
    }
}
