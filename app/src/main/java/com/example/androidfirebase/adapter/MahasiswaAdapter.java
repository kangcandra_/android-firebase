package com.example.androidfirebase.adapter;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.text.Layout;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.cardview.widget.CardView;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.recyclerview.widget.RecyclerView;

import com.example.androidfirebase.R;
import com.example.androidfirebase.mahasiswa.DialogForm;
import com.example.androidfirebase.model.Mahasiswa;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

import java.util.HashMap;
import java.util.List;

public class MahasiswaAdapter extends RecyclerView.Adapter<MahasiswaAdapter.MyViewHolder> {

    private List<Mahasiswa> mhsList;
    private Activity activity;

    DatabaseReference databaseReference = FirebaseDatabase.getInstance().getReference();

    public MahasiswaAdapter(List<Mahasiswa> mhsList, Activity activity){
        this.mhsList = mhsList;
        this.activity = activity;
    }

    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater layoutInflater = LayoutInflater.from(parent.getContext());
        View viewItem = layoutInflater.inflate(R.layout.layout_item, parent, false);
        return new MyViewHolder(viewItem);
    }

    @Override
    public void onBindViewHolder(@NonNull MyViewHolder holder, int position) {
        final Mahasiswa mhs = mhsList.get(position);
        holder.tv_nama.setText(mhs.getNama());
        holder.tv_nis.setText(mhs.getNis());
        holder.tv_jurusan.setText(mhs.getJurusan());
        holder.tv_alamat.setText(mhs.getAlamat());
        holder.btn_delete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                AlertDialog.Builder builder = new AlertDialog.Builder(activity);
                builder.setPositiveButton("Iya", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        databaseReference.child("Mahasiswa").child(mhs.getKey()).removeValue().addOnSuccessListener(new OnSuccessListener<Void>() {
                            @Override
                            public void onSuccess(Void unused) {
                                Toast.makeText(activity, "Data berhasil dihapus",Toast.LENGTH_SHORT).show();
                            }
                        }).addOnFailureListener(new OnFailureListener() {
                            @Override
                            public void onFailure(@NonNull Exception e) {
                                Toast.makeText(activity, "Data gagal dihapus!",Toast.LENGTH_SHORT).show();
                            }
                        });
                    }
                }).setNegativeButton("Tidak", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        dialogInterface.dismiss();
                    }
                }).setMessage("Apakah Anda Yakin Menghapus ? "+ mhs.getNama());
                builder.show();
            }
        });

        holder.card_hasil.setOnLongClickListener(new View.OnLongClickListener() {
            @Override
            public boolean onLongClick(View view) {
                FragmentManager fragmentManager = ((AppCompatActivity)activity).getSupportFragmentManager();
                DialogForm dialogForm = new DialogForm(
                        mhs.getNama(),
                        mhs.getNis(),
                        mhs.getJurusan(),
                        mhs.getAlamat(),
                        mhs.getKey(),
                        "Ubah"
                );
                dialogForm.show(fragmentManager, "form");
                return true;
            }
        });
    }

    @Override
    public int getItemCount() {
        return mhsList.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        TextView tv_nama, tv_nis, tv_jurusan, tv_alamat;
        CardView card_hasil;
        ImageView btn_delete;
        public MyViewHolder(@NonNull View itemView) {
            super(itemView);
            tv_nama = itemView.findViewById(R.id.tv_nama);
            tv_nis = itemView.findViewById(R.id.tv_nis);
            tv_jurusan = itemView.findViewById(R.id.tv_jurusan);
            tv_alamat = itemView.findViewById(R.id.tv_alamat);
            card_hasil = itemView.findViewById(R.id.card_hasil);
            btn_delete = itemView.findViewById(R.id.delete);
        }
    }
}
